package com.weatherlogger.app


object Constants {
const val PACKAGE_NAME = "com.weatherlogger.app"
    const val BASE_RETROFIT_URL = "https://api.openweathermap.org/"
    const val BASE_Image_URL = "https://openweathermap.org/img/w/"

    const val WEATHER_API_KEY = "bcc0f315fd65f0c019e7a897fcb0aab8"
    const val WEATHER_DATABASE_TABLE = "weather_table"
    const val WEATHER_DATABASE = "weather.db"

}