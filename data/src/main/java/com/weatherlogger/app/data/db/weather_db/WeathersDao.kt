package com.weatherlogger.app.data.db.weather_db

import androidx.lifecycle.LiveData
import androidx.room.*
import com.weatherlogger.app.Constants


@Dao
interface WeathersDao {
    /**
     * Wrapped around [LiveData] so when a new photo added to the db it will be observed
     * automatically at the UI to avoid pulling the db and have better lifecycle support.
     */
    @Query("SELECT * FROM ${Constants.WEATHER_DATABASE_TABLE}")
    fun loadWeatherRecords(): LiveData<MutableList<WeatherModel>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertWeatherRecord(weather: WeatherModel)

    @Update
    fun updateWeather(weather: WeatherModel)

    @Delete
    fun deleteWeather(weather: WeatherModel)
}