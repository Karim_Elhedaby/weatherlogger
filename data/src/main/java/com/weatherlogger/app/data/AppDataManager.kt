package com.weatherlogger.app.data

import com.weatherlogger.app.data.DataManager
import com.weatherlogger.app.data.api.retrofit.ApiHelperInterface
import com.weatherlogger.app.data.ui_models.WeatherResponce
import io.reactivex.Observable


class AppDataManager(var service: ApiHelperInterface) : DataManager {

    override fun getCurrentLocationWeatherData(
        lat: Double,
        lon: Double
    ): Observable<WeatherResponce> {
        return service.getCurrentLocationWeatherData(lat, lon)
    }
}