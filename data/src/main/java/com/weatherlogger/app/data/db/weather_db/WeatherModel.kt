package com.weatherlogger.app.data.db.weather_db

import android.os.Parcelable
import androidx.room.*
import com.weatherlogger.app.Constants
import kotlinx.android.parcel.Parcelize
import java.io.File
import java.sql.Blob

@Parcelize
@Entity(tableName = Constants.WEATHER_DATABASE_TABLE)
data class WeatherModel(
    @PrimaryKey(autoGenerate = true)
    var id: Int = 0,
    var placeName: String = "",
    var lat: Double = 0.0,
    var lang: Double = 0.0,
    var tempreture: Double = 0.0,
    var weatherIcon: String = "",
    @ColumnInfo(name = "weather_description")
    var weatherDescription: String = ""
) : Parcelable


