package com.weatherlogger.app.data.ui_models

data class Sys(
    val country: String,
    val message: Double,
    val sunrise: Int,
    val sunset: Int
)