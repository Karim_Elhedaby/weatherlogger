package com.weatherlogger.app.data

import android.util.Log
import androidx.lifecycle.LiveData
import com.weatherlogger.app.data.db.weather_db.WeatherDatabase
import com.weatherlogger.app.data.db.weather_db.WeatherModel
import com.weatherlogger.app.data.db.weather_db.WeathersDao
import java.lang.Exception
import java.util.concurrent.Executors

class DbRepository(val weatherDb: WeatherDatabase) : WeathersDao {

    private val executor = Executors.newSingleThreadExecutor()

    override fun loadWeatherRecords(): LiveData<MutableList<WeatherModel>> {
        return weatherDb.weatherersDao().loadWeatherRecords()
    }

    override fun insertWeatherRecord(weather: WeatherModel) {
        executor.execute {
            try {
                weatherDb.weatherersDao().insertWeatherRecord(weather)
            } catch (ex: Exception) {
                //TODO in production provide better error handling
                Log.e("weathersRepository", "Couldn't insert weather: $weather")
            }
        }
    }

    override fun updateWeather(weather: WeatherModel) {
        executor.execute {
            try {
                weatherDb.weatherersDao().updateWeather(weather)
            } catch (ex: Exception) {
                //TODO in production provide better error handling
                Log.e("weathersRepository", "Couldn't update weather: $weather")
            }
        }

    }

    override fun deleteWeather(weather: WeatherModel) {
        executor.execute {
            try {
                weatherDb.weatherersDao().deleteWeather(weather)
            } catch (ex: Exception) {
                //TODO in production provide better error handling
                Log.e("weathersRepository", "Couldn't delete weather: $weather")
            }
        }
    }

}

