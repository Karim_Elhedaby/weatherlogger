package com.weatherlogger.app.data.ui_models

data class Coord(
    val lat: Double,
    val lon: Double
)