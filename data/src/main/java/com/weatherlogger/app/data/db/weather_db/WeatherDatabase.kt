package com.weatherlogger.app.data.db.weather_db

import android.content.Context
import androidx.room.*
import com.weatherlogger.app.Constants.WEATHER_DATABASE
import com.weatherlogger.app.SingletonHolder

@Database(entities = [WeatherModel::class], version = 1)
    abstract class WeatherDatabase : RoomDatabase() {

    abstract fun weatherersDao(): WeathersDao

    companion object : SingletonHolder<WeatherDatabase, Context>({
        Room.databaseBuilder(it.applicationContext,
                WeatherDatabase::class.java, WEATHER_DATABASE)
                .build()
    })
}