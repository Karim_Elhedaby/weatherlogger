package com.weatherlogger.app.data.ui_models

data class Wind(
    val deg: Int,
    val speed: Double
)