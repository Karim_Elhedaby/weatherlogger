package com.weatherlogger.app.data.api.retrofit

import com.weatherlogger.app.Constants
import com.weatherlogger.app.data.ui_models.WeatherResponce
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ApiHelperInterface {
    /**
     *  Put your retrofit calls here
     */

    @GET("data/2.5/weather")
    fun getCurrentLocationWeatherData(
        @Query("lat") lat: Double,
        @Query("lon") lon: Double
    ): Observable<WeatherResponce>

}