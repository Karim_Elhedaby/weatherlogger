package com.weatherlogger.weatherlogger.di

import com.weatherlogger.app.ui.preview_weather.PreviewWeatherFragmentPresenter
import com.weatherlogger.app.ui.weather_records.WeatherRecordsViewModel
import org.koin.androidx.experimental.dsl.viewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val mainModule = module {
    viewModel { PreviewWeatherFragmentPresenter(dataManager = get(),repository = get()) }
    viewModel<WeatherRecordsViewModel>()
}