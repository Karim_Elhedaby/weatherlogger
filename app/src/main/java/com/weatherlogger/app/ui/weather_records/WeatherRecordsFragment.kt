package com.weatherlogger.app.ui.weather_records

import android.content.Intent
import android.os.Bundle
import android.provider.Settings
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import com.weatherlogger.app.Permission.isLocationServiceEnabled
import com.weatherlogger.app.R
import com.weatherlogger.app.data.DbRepository
import com.weatherlogger.app.data.db.weather_db.WeatherDatabase
import com.weatherlogger.app.data.db.weather_db.WeatherModel
import com.weatherlogger.app.replaceFragmentToActivityWithTransition
import com.weatherlogger.app.ui.preview_weather.PreviewWeatherFragment
import com.weatherlogger.app.ui.weather_records.adapter.WeatherRecordsAdapter
import com.weatherlogger.app.utils.secretB
import com.weatherlogger.app.utils.showB
import kotlinx.android.synthetic.main.back_toolbar.view.*
import kotlinx.android.synthetic.main.weather_records_fragment.*

class WeatherRecordsFragment : Fragment(), WeatherRecordsAdapter.RecordListener {

    var layoutManager: GridLayoutManager? = null
    var adapter: WeatherRecordsAdapter? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.weather_records_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        toolbar.titleTxtView.text = getString(R.string.weather_records)
        toolbar.backImageView.secretB()

        setUpController()

        val db = context?.let { WeatherDatabase.getInstance(it) }
        val factory = db?.let { DbRepository(it) }?.let { WeatherRecordsViewModelFactory(it) }

        val viewModel =
            ViewModelProviders.of(this, factory)
                .get(WeatherRecordsViewModel::class.java)

        viewModel.loadWeatherRecords().observe(
            viewLifecycleOwner, Observer {
                if (it.isNullOrEmpty()) emptyLayout.showB()
                else emptyLayout.secretB()
                adapter?.addMultipleItemsData(it)
            })
    }

    private fun setUpController() {
        addRecordFab.setOnClickListener {
            if (!isLocationServiceEnabled(requireContext())) {
                Toast.makeText(context, getString(R.string.open_location), Toast.LENGTH_SHORT)
                    .show()
                val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                startActivityForResult(intent, 100)
            } else {
                addNewRecord()
            }
        }
        setUpRecyclerView()
    }

    private fun setUpRecyclerView() {
        layoutManager = GridLayoutManager(context, 1)
        weatherRecordsRV?.layoutManager = layoutManager
        adapter = WeatherRecordsAdapter(mutableListOf(), this)
        weatherRecordsRV.adapter = adapter
    }

    private fun addNewRecord() {
        replaceFragmentToActivityWithTransition(
            parentFragmentManager
            , PreviewWeatherFragment.newInstance(),
            true
        )
    }


    override fun onClickRecord(weather: WeatherModel) {
        Toast.makeText(context, "The weather is " + weather.weatherDescription, Toast.LENGTH_SHORT)
            .show()
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)


        if (requestCode == 100 && resultCode == 0) {
            if (isLocationServiceEnabled(requireContext())) {
                addNewRecord()
            } else {
                Toast.makeText(context, "Please enable location", Toast.LENGTH_LONG)
                    .show()
            }
        }
    }


    companion object {
        fun newInstance() = WeatherRecordsFragment()
    }

}



