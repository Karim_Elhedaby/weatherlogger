package com.weatherlogger.app.ui.weather_records

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

import com.weatherlogger.app.data.DbRepository


/**
 * Factory class to create [WeatherRecordsViewModel] to inject the [WeatherRepository] to it's constructor
 * for better testing.
 */


class WeatherRecordsViewModelFactory(private val repository: DbRepository) : ViewModelProvider.Factory {
    @Suppress("unchecked_cast")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return WeatherRecordsViewModel(repository) as T
    }
}