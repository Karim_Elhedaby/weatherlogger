package com.weatherlogger.app.ui.preview_weather

import android.content.Intent
import android.graphics.Bitmap
import android.location.Location
import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.weatherlogger.app.Common.saveBitmap
import com.weatherlogger.app.Common.shareIt
import com.weatherlogger.app.Constants
import com.weatherlogger.app.Permission
import com.weatherlogger.app.R
import com.weatherlogger.app.base.BaseFragment
import com.weatherlogger.app.data.db.weather_db.WeatherModel
import com.weatherlogger.app.data.ui_models.WeatherResponce
import com.weatherlogger.app.utils.loadImage
import com.weatherlogger.app.utils.showB
import kotlinx.android.synthetic.main.back_toolbar.view.*
import kotlinx.android.synthetic.main.fragment_preview_image.*
import mumayank.com.airlocationlibrary.AirLocation
import org.koin.androidx.viewmodel.ext.android.viewModel

class PreviewWeatherFragment : BaseFragment<PreviewWeatherFragmentContract.Presenter>(),
        PreviewWeatherFragmentContract.View {

    private var currentLocation: AirLocation? = null

    override val presenter by viewModel<PreviewWeatherFragmentPresenter>()

    override fun getLayoutResource(): Int {
        return R.layout.fragment_preview_image
    }

    override fun initLayout(savedInstanceState: Bundle?, view: View) {
        presenter.attachView(this, lifecycle)

        toolbar.backImageView.setOnClickListener {
            parentFragmentManager.popBackStack()
        }
        toolbar.titleTxtView.text = getString(R.string.weather_info)

        setUpGetLocationController()
    }


    private fun setUpShareActionController() {

        Permission.checkStorageAccessPermission(
                requireActivity(),
                object : Permission.Callback {
                    override fun isPermissionAccepted(isAccepted: Boolean?) {
                        if (isAccepted == true) {

                            val bitmap = takeScreenshot()
                            bitmap?.let { saveBitmap(it) }
                            context?.let { shareIt(it) }
                        } else {
                            Toast.makeText(
                                    context,
                                    "Permission not accepted",
                                    Toast.LENGTH_SHORT
                            ).show()
                        }
                    }
                })
    }


    private fun takeScreenshot(): Bitmap? {
        val rootView = layout
        rootView.isDrawingCacheEnabled = true
        return rootView.drawingCache
    }

    private fun setUpGetLocationController() {
        currentLocation = activity?.let {
            AirLocation(
                    it,
                    shouldWeRequestPermissions = true, shouldWeRequestOptimization = true,
                    callbacks = object : AirLocation.Callbacks {
                        override fun onSuccess(location: Location) {
                            presenter.getWeather(location.latitude, location.longitude)
                        }

                        override fun onFailed(locationFailedEnum: AirLocation.LocationFailedEnum) {
                            Toast.makeText(context, locationFailedEnum.name, Toast.LENGTH_LONG)
                                    .show()
                        }
                    })
        }
    }

    override fun onGetWeatherSuccessfully(weatherResponce: WeatherResponce) {
        saveB.showB()
        shareFab.showB()
        fillWeatherData(weatherResponce)

        saveB.setOnClickListener {
            setUpSaveRecordController(weatherResponce)
            parentFragmentManager.popBackStack()
        }

        shareFab.setOnClickListener {
            setUpShareActionController()
        }

    }

    private fun fillWeatherData(weatherResponce: WeatherResponce) {
        placeNameTV.text = weatherResponce.name
        tempretureTV.text = """${weatherResponce.main.temp}°"""
        val iconUrl = Constants.BASE_Image_URL + weatherResponce.weather[0].icon + ".png"
        weatherIconIV.loadImage(iconUrl)
        weatherDescriptionTV.text = weatherResponce.weather[0].description
    }

    private fun setUpSaveRecordController(weatherResponce: WeatherResponce) {
        presenter.addWeatherRecord(
                WeatherModel(
                        placeName = weatherResponce.name,
                        lang = weatherResponce.coord.lon,
                        lat = weatherResponce.coord.lon,
                        weatherDescription = weatherResponce.weather[0].description,
                        tempreture = weatherResponce.main.temp,
                        weatherIcon = weatherResponce.weather[0].icon
                )
        )
    }

    override fun onGetWeatherError(errorMsg: String) {
        Toast.makeText(context, errorMsg, Toast.LENGTH_SHORT).show()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        currentLocation?.onActivityResult(
                requestCode,
                resultCode,
                data
        )
        super.onActivityResult(requestCode, resultCode, data)
    }

    override fun onRequestPermissionsResult(
            requestCode: Int,
            permissions: Array<out String>,
            grantResults: IntArray
    ) {

        currentLocation?.onRequestPermissionsResult(
                requestCode,
                permissions,
                grantResults
        )

        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }


    override fun onResume() {
        super.onResume()
        if (!presenter.isViewAttached()) {
            presenter.attachView(this, lifecycle)
        }
    }

    companion object {
        fun newInstance() = PreviewWeatherFragment()
    }

}
