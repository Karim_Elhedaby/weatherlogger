package com.weatherlogger.app.ui.weather_records.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.weatherlogger.app.R
import com.weatherlogger.app.data.db.weather_db.WeatherModel
import com.weatherlogger.app.utils.Multimedia.getBitmapImage
import com.weatherlogger.app.utils.loadImage
import kotlinx.android.synthetic.main.item_weather_record.view.*


class WeatherRecordsAdapter(
    val data: MutableList<WeatherModel>,
    private val listener: RecordListener
) : RecyclerView.Adapter<WeatherRecordsAdapter.WeatherRecordsAdapterViewHolder>() {

    class WeatherRecordsAdapterViewHolder(
        itemView: View, val data: MutableList<WeatherModel>,
        private val listener: RecordListener
    ) : RecyclerView.ViewHolder(itemView) {

        init {
            itemView.setOnClickListener {
                listener.onClickRecord(data[adapterPosition])
            }

        }


        fun bind(item: WeatherModel) = with(itemView) {

            placeNameTV.text = item.placeName
            weatherDescriptionTV.text = item.weatherDescription
            weatherTemperatureTV.text = """${item.tempreture}°"""
        }
    }


    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): WeatherRecordsAdapterViewHolder {
        return WeatherRecordsAdapterViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.item_weather_record, null),
            data, listener
        )
    }

    override fun onBindViewHolder(holder: WeatherRecordsAdapterViewHolder, position: Int) =
        holder.bind(data[position])


    fun addMultipleItemsData(receivedData: MutableList<WeatherModel>) {
        data.addAll(receivedData)
        notifyDataSetChanged()
    }

    fun swapData(receivedData: MutableList<WeatherModel>) {
        data.clear()
        data.addAll(receivedData)
        notifyDataSetChanged()
    }

    fun addSingleItemDataFromLast(receivedData: WeatherModel) {
        data.add(receivedData)
        notifyItemInserted(data.size)
    }

    fun removeAllData() {
        data.clear()
        notifyDataSetChanged()
    }

    interface RecordListener {
        fun onClickRecord(weather: WeatherModel)
    }

    override fun getItemCount(): Int {
        return data.size
    }

}

