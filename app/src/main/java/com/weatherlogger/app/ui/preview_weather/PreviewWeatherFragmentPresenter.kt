package com.weatherlogger.app.ui.preview_weather

import com.weatherlogger.app.base.BasePresenter
import com.weatherlogger.app.data.DataManager
import com.weatherlogger.app.data.db.weather_db.WeatherModel
import com.weatherlogger.app.data.db.weather_db.WeathersDao
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers


class PreviewWeatherFragmentPresenter(
    private val dataManager: DataManager,
    private var repository: WeathersDao
) : BasePresenter<PreviewWeatherFragmentContract.View>(),
    PreviewWeatherFragmentContract.Presenter {

    override fun getWeather(lat: Double, long: Double) {
        view?.showLoading()
        compositeDisposable.addAll(
            dataManager.getCurrentLocationWeatherData(lat, long)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeBy(
                    onNext = {
                        view?.hideLoading()
                        if (it.cod == 200) {
                            view?.onGetWeatherSuccessfully(it)
                        } else {
                            view?.onGetWeatherError("Error retrieve weather data")
                        }
                    },
                    onError = {
                        view?.hideLoading()
                        view?.onNetworkError()
                    })
        )
    }

    override fun addWeatherRecord(weatherModel: WeatherModel) {
        return repository.insertWeatherRecord(weatherModel)
    }

}