package com.weatherlogger.app.ui.main

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.weatherlogger.app.R
import com.weatherlogger.app.replaceFragmentToActivity
import com.weatherlogger.app.ui.weather_records.WeatherRecordsFragment


class MainActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        replaceFragmentToActivity(
            supportFragmentManager,
            WeatherRecordsFragment.newInstance(), false
        )

    }

}
