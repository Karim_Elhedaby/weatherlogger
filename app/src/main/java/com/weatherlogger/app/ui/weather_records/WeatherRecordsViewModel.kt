package com.weatherlogger.app.ui.weather_records

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.weatherlogger.app.data.DbRepository
import com.weatherlogger.app.data.db.weather_db.WeatherModel

class WeatherRecordsViewModel(private var repository: DbRepository) : ViewModel() {


    fun loadWeatherRecords(): LiveData<MutableList<WeatherModel>> {
        return repository.loadWeatherRecords()
    }


}
