package com.weatherlogger.app.ui.preview_weather

import com.weatherlogger.app.base.MvpPresenter
import com.weatherlogger.app.base.MvpView
import com.weatherlogger.app.data.db.weather_db.WeatherModel
import com.weatherlogger.app.data.ui_models.WeatherResponce

interface PreviewWeatherFragmentContract {

    interface Presenter : MvpPresenter<View> {
        fun getWeather(lat: Double, long: Double)
        fun addWeatherRecord(weatherModel: WeatherModel)
    }

    interface View : MvpView {
        fun onGetWeatherSuccessfully(weatherResponce: WeatherResponce)
        fun onGetWeatherError(errorMsg: String)
    }
}