package com.weatherlogger.app

import android.app.Application
import com.weatherlogger.app.di.dataModule
import com.weatherlogger.app.utils.BuildConfig
import com.weatherlogger.app.utils.ReleaseTree

import com.weatherlogger.weatherlogger.di.mainModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import timber.log.Timber

class App : Application() {
    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidContext(this@App)
            androidLogger()
            modules(listOf(dataModule, mainModule))
        }

        if (BuildConfig.DEBUG) {

            Timber.plant(Timber.DebugTree())
        } else {
            Timber.plant(ReleaseTree())
        }
    }
}