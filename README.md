# WeatherLogger
A Weather application based on openweathermap-api that let the users to add current weather information (e.g. place name, temperature, weather condition, …) then record locally and can  share it.   


# Features

* mvvm-architecture 
* mvp-architecture
* repository-pattern
* koin
* viewmodel-pattern
* rxjava2
* retrofit
* room
* room-database
* livedata-viewmodel
* openweathermap-api
* openweathermap-android
* lifecycle
* lifecycleobserver
* weather-application 